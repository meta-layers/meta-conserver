DESCRIPTION = "Conserver provides remote access to serial port \
consoles and logs all data to a central host."
HOMEPAGE = "http://www.conserver.com/"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b28513e7b696027d3d2b8dbf117f9fe5"

SRC_URI = "git://git@github.com/bstansell/conserver.git;protocol=http;branch=master \
           ${@bb.utils.contains('DISTRO_FEATURES', 'systemd', 'file://volatiles.conserver.conf', 'file://volatiles.99_conserver', d)} \
           file://console.cf \
           file://conserver.cf \
           file://conserver.passwd \
           file://conserver.service \
           file://console-autocomp.bash \
"
#SRCREV = "c8355ae8b9691f02e9b76fe74397b2fa9d8c3562"
#SRCREV = "${AUTOREV}"
#SRCREV = "8b3dfdb14f339b2d2f01dda43395a4e1f4fbdc79"
SRCREV = "31bdc9b4dd070b9d2e0d755de21f564d583ff084"



# it's not version 8.2.6 but something newer 
# to make it compile with recent yocto version
#PV = "8.2.6"
# only like the PRSRV can increment the package version
# if we change something in the recipe:
PV = "8.2.7+git${SRCPV}"

S = "${WORKDIR}/git"

# Use autotools-brokensep to prevent separation of source and build dirs, i.e.,
# build in the source dir...
inherit autotools-brokensep

# Requires libcrypt for linking. Use extended crypt library package
DEPENDS += "libxcrypt"

# remove this dependency if we compile with libc-musl
# it is supposed to come from musl
DEPENDS:remove:libc-musl = "libxcrypt"

# Runtime dependency on 
# bash-completion
# netbase: port added to /etc/services
# udev-rules-usb-serial: udev rules for usb serial devices
RDEPENDS:${PN}:class-target += "bash-completion netbase udev-rules-usb-serial"

# Stop install being called with -s so it doesn't try and run the host strip command
HOSTTOOLS = "install"
EXTRA_OEMAKE = "INSTALL_PROGRAM=install"

# Packages
PACKAGES = "${PN} ${PN}-dbg ${PN}-doc"
PROVIDES = "${PN} ${PN}-dbg ${PN}-doc"

# Package doc files
FILES:${PN}-doc += "/usr/share"

# Install default configuration files
do_install:append:class-target() {
    # Create directories
    install -d ${D}${sysconfdir}
    install -d ${D}${sysconfdir}/bash_completion.d
    install -d ${D}${systemd_system_unitdir}
    # Install sources
    install -m 0644 ${UNPACKDIR}/console.cf ${D}${sysconfdir}/console.cf
    install -m 0644 ${UNPACKDIR}/conserver.cf ${D}${sysconfdir}/conserver.cf
    install -m 0644 ${UNPACKDIR}/conserver.passwd ${D}${sysconfdir}/conserver.passwd
    install -m 0644 ${UNPACKDIR}/console-autocomp.bash ${D}${sysconfdir}/bash_completion.d/console-autocomp.bash

    if [ -f ${UNPACKDIR}/${cfg-addon} ]; then
       cat ${UNPACKDIR}/${cfg-addon} >> ${D}${sysconfdir}/conserver.cf
    fi

    # Only install the script if 'sysvinit' is in DISTRO_FEATURES
    # systemd would be the other choice
    if ${@bb.utils.contains('DISTRO_FEATURES','sysvinit','true','false',d)}; then
       # sys-v service
       # @@@ missing
       
       # volatile log files
       install -d ${D}${sysconfdir}/default/volatiles
       install -m 644 ${UNPACKDIR}/volatiles.99_conserver ${D}${sysconfdir}/default/volatiles/99_conserver
    fi

    # Only install the script if 'systemd' is in DISTRO_FEATURES
    # systemd
    if ${@bb.utils.contains('DISTRO_FEATURES', 'systemd','true','false',d)}; then
       # systemd service
       install -m 0644 ${UNPACKDIR}/conserver.service ${D}${systemd_system_unitdir}

       # volatile log files
       install -d ${D}${sysconfdir}/tmpfiles.d
       install -m 644 ${UNPACKDIR}/volatiles.conserver.conf ${D}${sysconfdir}/tmpfiles.d/conserver.conf
    fi
}

#SYSTEMD_SERVICE:${PN} = "conserver.service"
#PACKAGE_ARCH = "${MACHINE_ARCH}"

# --> systemd service
# please note, that above we already copy files depeding on sysvinit/systemd
inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
# disable for manual testing
# e.g. on target:
# systemctl start telegraf.service
# SYSTEMD_AUTO_ENABLE = "disable"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "${PN}.service"
# <-- systemd service

BBCLASSEXTEND="native"
#DEPENDS:append:class-native = " bash-completion"
