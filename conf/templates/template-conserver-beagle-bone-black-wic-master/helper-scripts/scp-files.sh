#!/bin/bash

DEPLOY="conserver-beagle-bone-black-wic-master/tmp/deploy/images/beagle-bone-black"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-base*-bone-black-*.rootfs.wic*"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
set +x

popd
