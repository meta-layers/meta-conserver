#!/bin/bash

DEPLOY="conserver-beagle-bone-black-wic-swupdate-master/tmp/deploy/images/beagle-bone-black-conserver"

pushd /workdir/build/${DEPLOY}

SOURCE_1="core-image-minimal-base-conserver-beagle-bone-black-conserver*??????????????*.wic*"
SOURCE_2="update-image-conserver-beagle-bone-black-conserver-consrv-*swu"

SOURCE_3="core-image-minimal-base-conserver-devel-beagle-bone-black-conserver*??????????????*.wic*"
SOURCE_4="update-image-conserver-devel-beagle-bone-black-conserver-consrv-*swu"

TARGET_1="student@192.168.42.60:/home/student/projects/${DEPLOY}"

set -x
scp -r ${SOURCE_1} ${TARGET_1}
scp -r ${SOURCE_2} ${TARGET_1}
scp -r ${SOURCE_3} ${TARGET_1}
scp -r ${SOURCE_4} ${TARGET_1}

set +x

popd
