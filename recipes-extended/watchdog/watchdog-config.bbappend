FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# Install default configuration files
do_install:append() {
    if [ -f ${WORKDIR}/${cfg-addon} ]; then
       cat ${WORKDIR}/${cfg-addon} >> ${D}${sysconfdir}/watchdog.conf
    fi
}
