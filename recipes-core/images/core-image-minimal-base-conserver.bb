# Copyright (C) 2021 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

require recipes-core/images/core-image-minimal-base.bb

IMAGE_INSTALL += "\
                  telegraf \
                 "

IMAGE_INSTALL += "\
                  conserver \
                 "

IMAGE_INSTALL += "\
                  screen \
                  byobu \
                 "

# byobo seems to require this:
IMAGE_INSTALL += "\
                  glibc-utils \
                  localedef \
                 "

IMAGE_INSTALL += "\
                  usbutils \
                 "

IMAGE_FEATURES += "package-management"

# ---------------------------------------------------------------------------------

# essential stuff for swupdate
IMAGE_INSTALL += " swupdate \
                   swupdate-www \
                   swupdate-tools-ipc \
                   swupdate-progress \
                   libubootenv \
                   libubootenv-bin \
                   parted \
                   util-linux-findmnt \
                   util-linux-sfdisk \
                   mountmnt \
                   togglerootfspartition \
                 "

# non essential stuff for swupdate
#IMAGE_INSTALL += " swupdate-lualoader \
#                 "

IMAGE_INSTALL += " device-tree \
                 "

# tests
IMAGE_INSTALL += " sys-class-leds-test \
                   yum-test \
                   libgpiod \
                   libgpiod-tools \
                   i2c-tools \
                   evtest \
                   gpio-input \
                   libevdev \
                 "

# app
IMAGE_INSTALL += " mosquitto \
                   mosquitto-clients \
                   paho-mqtt-c \
                   sysfsutils \
                   mqtt-key-event-daemon \
                   reset-usb-hub \
                 "

# attempt to try zlog for app
IMAGE_INSTALL += " zlog \
                 "

# additional stuff - if not already there
IMAGE_INSTALL += " htop \
                   vim \
                   bash \
                   dropbear \
                   rng-tools \
                   coreutils \
                   shellinabox \
                   xz \
                   bmaptool \
                 "

# where is the bmap-tools recipe?
# bmap-tools -> bmaptool

# usbip + usb additions
IMAGE_INSTALL += " usbutils \
                   usbip-tools \
                 "

