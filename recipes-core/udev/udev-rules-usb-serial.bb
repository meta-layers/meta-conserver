DESCRIPTION = "udev rules for usb serial devices e.g. for conserver"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " file://99-usb-serial.rules"
SRC_URI:append = " ${@bb.utils.contains('HOST_NAME', 'consrv-plus-6', 'file://${extra-usb-serial-rules-file}', '', d)}"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

do_install () {
	install -d ${D}${sysconfdir}/udev/rules.d
	install -m 0644 ${UNPACKDIR}/99-usb-serial.rules ${D}${sysconfdir}/udev/rules.d/
        if [ -f ${UNPACKDIR}/${extra-usb-serial-rules-file} ]; then
          install -m 0644 ${UNPACKDIR}/${extra-usb-serial-rules-file} ${D}${sysconfdir}/udev/rules.d/
        fi
}
