# conserver needs some port in order to work:
do_install:append() {
        # add conserver port after this line:
        #     Local services
        #     this (/etc/services) is valid for sys-v and systemd
        sed -i '/^# Local services/a conserver       3109/tcp        CONSERVER_PORT  # Conserver' ${D}${sysconfdir}/services
}
