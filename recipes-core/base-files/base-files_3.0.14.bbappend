# version and machine specific changes:
FILESEXTRAPATHS:prepend := "${THISDIR}/${PV}/${MACHINE}:"

# for byobu we need ${sysconfdir}/skel/.byobu/windows
do_install:append() {
    if [ -f ${WORKDIR}/${windows-cfg-file} ]; then
       install -d ${D}${sysconfdir}/skel/.byobu
       install -m 0755 ${WORKDIR}/${windows-cfg-file} ${D}${sysconfdir}/skel/.byobu/windows
    fi
}

# I put the comment out of the below snippet otherwise it throws the QA Issue ;)
# QA Issue: pkg_postinst in base-files recipe contains ${D}, 
# it should be replaced by $D instead [expanded-d]

pkg_postinst:${PN}:append () {
   if [ -f ${WORKDIR}/${windows-cfg-file} ]; then
      install -d $D${ROOT_HOME}/.byobu
      install -m 0600 $D${sysconfdir}/skel/.byobu/windows $D${ROOT_HOME}/.byobu/windows
   fi
}
