cd /sys/class/leds/usbswitch:usbout0

# to switch on

     50 ms
     _____
_____     _____


echo oneshot > trigger


# 20 Hz -> 50 ms

echo oneshot > trigger # set trigger for this led
echo 20 > delay_on     # blink at 1 / (20 + 20) Hz on continuous traffic
echo 20 > delay_off

echo 1 > shot # led starts blinking, ignored if already blinking


# to switch off

     1 s
     _____
_____     _____


echo oneshot > trigger # set trigger for this led
echo 2 > delay_on     # blink at 1 / (1 + 1) Hz on continuous traffic
echo 2 > delay_off

echo 1 > shot # led starts blinking, ignored if already blinking

does not quite work!!!

----


better to toggle:

echo 1 > brightness && sleep 2 && echo 0 > brightness

----

not-usb-power	not-usb-cable
=================================================================================================================================
active		active		this is the default - how the system boots
				e.g.
				[   18.822780] usb 1-1.1.1: new full-speed USB device number 4 using musb-hdrc
				[   19.036781] ftdi_sio 1-1.1.1:1.0: FTDI USB Serial Device converter detected
				[   19.044142] usb 1-1.1.1: Detected FT232RL
				[   19.137749] usb 1-1.1.1: FTDI USB Serial Device converter now attached to ttyUSB0
active		inactive	it's like disconnecting the active USB cables from the Atolla
				e.g.
				[  443.374698] usb 1-1.1: USB disconnect, device number 3
				[  443.379943] usb 1-1.1.1: USB disconnect, device number 4
				[  443.394066] ftdi_sio ttyUSB0: FTDI USB Serial Device converter now disconnected from ttyUSB0
				[  443.402995] ftdi_sio 1-1.1.1:1.0: device disconnected
inactive	active		it's like connecting the active USB cables from the Atolla
                                e.g.
                                [   18.822780] usb 1-1.1.1: new full-speed USB device number 4 using musb-hdrc
                                [   19.036781] ftdi_sio 1-1.1.1:1.0: FTDI USB Serial Device converter detected
                                [   19.044142] usb 1-1.1.1: Detected FT232RL
                                [   19.137749] usb 1-1.1.1: FTDI USB Serial Device converter now attached to ttyUSB0
inactive	inactive	this is like disconnecting the USB cable and the power from the Atolla USB hub
=================================================================================================================================

