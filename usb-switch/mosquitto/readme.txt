-----

mosquitto_sub -h localhost -t /sys/class/leds/usbswitch:green1

mosquitto_pub -h localhost  -t /sys/class/leds/usbswitch:green1 -m "brightness/1"

root@consrv-1:~# mosquitto_sub -h localhost -t /sys/class/leds/usbswitch:green1
brightness/1

-----

/etc/mosquitto/mosquitto.conf:

listener 1883 0.0.0.0
allow_anonymous true

systemctl restart mosquitto

mosquitto_sub -h ${HOSTNAME} -t /sys/class/leds/usbswitch:green1

mosquitto_pub -h ${HOSTNAME} -t /sys/class/leds/usbswitch:green1 -m "brightness/1"

Now we can also use MQTT Explorer from a machine on the same network and sniff/publish messages

-----

SDK:

in cooker mode:

pokyuser@HP-ZBook-15-G4-2:/workdir/build/conserver-beagle-bone-black-wic-swupdate-master$
bitbake meta-ide-support
bitbake -c populate_sysroot
bitbake build-sysroots

/workdir/build/conserver-beagle-bone-black-wic-swupdate-master/tmp/deploy/images/beagle-bone-black-conserver

source environment-setup-armv7at2hf-neon-resy-linux-gnueabi

### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal-base-conserver
    update-image-conserver

You can also run generated qemu images with a command like 'runqemu qemux86'

-----





