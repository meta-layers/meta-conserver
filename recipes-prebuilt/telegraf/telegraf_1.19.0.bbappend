# telegraf version and machine specific changes:
FILESEXTRAPATHS:prepend := "${THISDIR}/${PV}/${MACHINE}:"

do_install:append () {
   # overwrite config file with custom config file, if it exists
   if [ -f ${WORKDIR}/${cfg-file} ]; then
      install -m 0644 ${WORKDIR}/${cfg-file} ${D}${sysconfdir}/telegraf/telegraf.conf
   fi
}
